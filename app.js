const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')
const http = require('http')
const { Server } = require('socket.io')

app.use(cors())
const server = http.createServer(app)

const io = new Server(server, {
    cors: {
        origin: '*'
    }
})

io.on('connection', (socket) => {
    console.log('user connected', socket.id)

    socket.on('join', (data) => {
        socket.join(data[0]._id)
        console.log(`users ${data[0].users} joined room ${data[0]._id}`)
    })

    socket.on('send', (data) => {
        socket.to(data.room).emit('receive', data)
    })


    socket.on('disconnect', () => {
        console.log('user disconnected', (socket.id))
    })
})


app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Routes
const userRoute = require('./Router/userRouter')
app.use('/user', userRoute)
const chatRoute = require('./Router/chatRouter')
app.use('/chat', chatRoute)


mongoose.connect(`mongodb+srv://admin:admin@wdc028-course-booking.mgfy3.mongodb.net/chat-app?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, () => console.log(`Connected to MongoDB`))

server.listen(process.env.PORT || 3001, () => {
    console.log('server running')
})