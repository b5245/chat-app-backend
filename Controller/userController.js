const User = require('../Model/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const { createAccessToken } = require('../auth')

module.exports = {

    create: (req, res) => {
        const newUser = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            username: req.body.username,
            password: bcrypt.hashSync(req.body.password, 10)
        })

        newUser.save().then(result => {
            res.send(result)
        })
    },

    checkUser: (req, res) => {
        User.aggregate([{
            $match: {
                username: req.body.username
            }
        }]).then(result => {
            res.send(result)
        })
    },

    get: (req, res) => {
        User.find().then(result => {
            res.send(result)
        })
    },

    login: async (req, res) => {
        try {
            await User.findOne({ username: req.body.username }).then(result => {

                const passMatch = bcrypt.compareSync(req.body.password, result.password)
                if (passMatch) {
                    res.send({ access: createAccessToken(result) })
                }
                else {
                    res.send(false)
                }
            })
        }
        catch {
            res.send(false)
        }

    },

    userDetails: (req, res) => {
        const userData = auth.decode(req.headers.authorization)

        User.findById(userData.id).then(result => {
            res.send(result)
        })
    },

    search: (req, res) => {
        User.aggregate([{
            $match: {
                $or: [
                    { username: req.body.search },
                    { firstName: req.body.search },
                    { lastName: req.body.search }
                ]
            }
        }]).then(result => {
            res.send(result)
        })
    },

    adduser: (req, res) => {
        const userData = auth.decode(req.headers.authorization)

        User.findById(req.body.id).then(result1 => {
            User.findById(userData.id).then(result2 => {
                result2.contact.push({
                    username: result1.username,
                    firstName: result1.firstName,
                    lastName: result1.lastName
                })

                result2.save().then((succ, err) => {
                    if (err) {
                        res.send(err)
                    }
                    else {
                        res.send(succ)
                    }
                })
            })
        })



    }
}