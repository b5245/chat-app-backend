const e = require('express')
const Chat = require('../Model/Chat')

module.exports = {

    createChat: (req, res) => {
        const newChat = new Chat({
            users: [req.body.me, req.body.friend]
        })
        newChat.save().then((succ, err) => {
            if (err) {
                res.send(err)
            }
            else {
                res.send(succ)
            }
        })
    },

    getAll: (req, res) => {
        Chat.find().then(result => {
            res.send(result)
        })
    },

    findChat: (req, res) => {
        const me = req.body.me
        const friend = req.body.friend
        Chat.find({
            $and: [
                { users: me, friend },
                { users: friend, me }
            ]
        }).then(result => {
            if (result.length == 0) {
                res.send(false)
            }
            else {
                res.send(result)
            }
        })
    },

    sendMessage: (req, res) => {
        Chat.findById(req.body.chatid).then(result => {
            result.conversation.push({
                me: req.body.me,
                friend: req.body.friend,
                room: req.body.room,
                message: req.body.message,
                time: req.body.time
            })

            result.save().then((succ, err) => {
                if (err) {
                    req.send(err)
                }
                else[
                    res.send(succ)
                ]
            })
        })
    }

}