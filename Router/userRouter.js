const express = require('express')
const router = express.Router()
const userController = require('../Controller/userController')
const auth = require('../auth')

router.post('/create', userController.create)

router.post('/checkuser', userController.checkUser)

router.get('/getusers', userController.get)

router.post('/login', userController.login)

router.get('/userdetails', auth.verify, userController.userDetails)

router.post('/search', userController.search)

router.post('/add', auth.verify, userController.adduser)

module.exports = router
