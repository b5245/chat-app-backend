const express = require('express')
const router = express.Router()
const chatController = require('../Controller/chatController')


router.post('/createchat', chatController.createChat)

router.get('/getallchat', chatController.getAll)

router.post('/findchat', chatController.findChat)

router.patch('/sendmessage', chatController.sendMessage)

module.exports = router