const mongoose = require(`mongoose`)

const chatSchema = mongoose.Schema({
    users: {
        type: Array,
    },
    conversation: [
        {
            me: {
                type: String
            },
            friend: {
                type: String
            },
            room: {
                type: String
            },
            message: {
                type: String
            },
            time: {
                type: String
            }
        }
    ]


})

module.exports = mongoose.model(`Chat`, chatSchema)